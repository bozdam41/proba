package com.iwec.myebooks;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MyebooksApplication {

	public static void main(String[] args) {
		SpringApplication.run(MyebooksApplication.class, args);
	}

}
