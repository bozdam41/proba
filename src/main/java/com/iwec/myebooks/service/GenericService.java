package com.iwec.myebooks.service;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Transactional(readOnly = false, propagation = Propagation.SUPPORTS)
public interface GenericService<T> {
	public List<T> findAll();
	public T findById(Integer id);
	public T save(T t);
	public T update(T t);
	public void delete(Integer id);
}
